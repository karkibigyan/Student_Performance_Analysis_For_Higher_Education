<?php defined('BASEPATH') OR exit('No direct script access allowed.');
class Year_controller extends Base_Authenticated_Controller {

    public function __construct() {

        parent::__construct();

        $this->_set_nav('class');
        $this->load->library('grocery_crud');
    }

    public function index()
    {
    	$title = 'Year';
    	$this->registry->set('title', $title);
    	$this->_set_header_icon('dashboard');
    	$crud = new grocery_CRUD();
    	$crud->set_table('year');
    	$crud->set_subject('Year');
    	$output = $crud->render();
    	$this->template
    	    ->prepend_title($title)
    	    ->set(compact('title'))
    	    ->set('output',$output->output)
    	    ->set('css_files',$output->css_files)
    	    ->set('js_files',$output->js_files)
    	    ->build('index');
    }
}