<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// use Phpml\Metric\Accuracy;
use Phpml\Classification\NaiveBayes;
use Phpml\Metric\Accuracy;
use Phpml\Metric\ConfusionMatrix;
use Phpml\Clustering\KMeans;
use Phpml\Clustering\DBSCAN;

class Predict_controller extends Base_Authenticated_Controller {

	public function __construct(){
		parent::__construct();
	}
	public function index($id)
	{
		
		$title = 'Result - Student Performance Analysis';
		$this->load->model('student');
		$columns = $this->student->getColumns();
		$predict = $this->student->getPredict($id);

		$accuracy = $this->student->getAccuracy($id);
		$hline = $this->student->getHistoryLineGraph($id);
		$students = $this->student->predictetdResult($id);
		// var_dump($students);
		// var_dump($accuracy,$hline);
        $this->registry->set('title', $title);
        $this->_set_header_icon('dashboard');
		$this->template
		->set(compact('columns','accuracy','predict','hline','students'))
		->set_partial('scripts', 'home_scripts')
		->build('newpredict');
	}
	public function index_old($id)
	{
		$d = $this->start($id);
		$kmean = json_encode($d,true);
		$title = 'Predict - Student Performance Analysis';
        $this->registry->set('title', $title);
        $this->_set_header_icon('dashboard');
		$this->template
		->set(compact('kmean'))
		->build('predict');
	}
	public function start($semesterID)
	{
		$data = $this->getData($semesterID);
		$classifier = new NaiveBayes();
        $classifier->train($data['train'], $data['label']);
        $p = $classifier->predict($data['test']);

		$kmeans = new KMeans(5);
		        $r = $kmeans->cluster($data['newtrain']);
		        $e = $this->prepareCluster($r);

		//         $dbscan = new DBSCAN($epsilon = 2, $minSamples = 3);
		//         $q = $dbscan->cluster($data['train']);

		// exit();
		return $e;
	}
	public function prepareCluster($data)
	{
		$t = [];
		$t[] = array('Cluster','Marks');
		foreach ($data as $index => $d) {
			foreach ($d as $l) {
				array_pop($l);
				foreach ($l as $s) {
					$index = array_sum($l);
					$t[] = array((int)$index,(int)$s);
				}
			}
		}

		return $t;
	}
	public function getData($semesterID)
	{
		$data =  $this->db->where('semester_id',$semesterID)->get('student_data')->result();
		$result = $this->db->select('student_id,result')->where('semester_id',$semesterID)->where('result is NOT NULL', NULL, FALSE)->get('predicted_result')->result();
		$result = array_column($result, 'result','student_id');
		$t = [];
		foreach ($data as $d) {
			if ($d->type=='test') {
				$t['test'][$d->student_id][] = $d->value;
			}else{
				$t['train'][$d->student_id][] = $d->value;
			}
		}
		foreach ($t['test'] as $i => $o) {
			$students[] = $i;
			$test[] = $o;
		}
		foreach ($t['train'] as $i => $o) {
			$o['result'] = ( strtolower( $result[$i])=='no') ? 0 : 1 ;
			$o = array_values($o);
			$newtrain[] = $o;
		}
		$t['test'] = $test;
		$t['label'] = $result;
		$t['newtrain'] = $newtrain;
		$t['students'] = $students;
		return $t;
	}

}

/* End of file Predict_controller.php */
/* Location: ./applications/admin/modules/home/controllers/Predict_controller.php */