<?php defined('BASEPATH') OR exit('No direct script access allowed.');
// use Phpml\Metric\Accuracy;
use Phpml\Classification\NaiveBayes;
use Phpml\Metric\Accuracy;
use Phpml\Metric\ConfusionMatrix;
use Phpml\Clustering\KMeans;

class Home_controller extends Base_Authenticated_Controller {

    public function __construct() {

        parent::__construct();

        $this->_set_nav('home');
        $this->load->model('student');
    }
    public function test()
    {
        $name = 'train01.csv';
        $csvFile = DEFAULTFCPATH."/" . $name;
        $file_handle = fopen($csvFile, 'r');
        while (!feof($file_handle)) {
            $line_of_text[] = fgetcsv($file_handle, 2048);
        }
        fclose($file_handle);
        var_dump($line_of_text);
        //$this->student->add($line_of_text,$semester,$type);
    }
    public function process($name,$semester,$type)
    {
        // $storagename = $name;
        $csvFile = DEFAULTFCPATH."/" . $name;
        $file_handle = fopen($csvFile, 'r');
        while (!feof($file_handle)) {
            $line_of_text[] = fgetcsv($file_handle, 2048);
        }
        fclose($file_handle);
        // var_dump($line_of_text);
        // exit();
        $this->student->add($line_of_text,$semester,$type);
    }
    public function accuracy()
    {
        $a = $this->input->post('predicted');
        $b = $this->input->post('result');

        $c = Accuracy::score($b, $a);
        $d = Accuracy::score($b, $a,false);
        echo $c." | Normalized:- ".$d;

         $confusionMatrix = ConfusionMatrix::compute($b, $a);
        echo '<br><br><table border="1">';
        foreach( $confusionMatrix as $m )
        {
            echo '<tr>';
            foreach( $m as $key )
            {
                echo '<td>'.$key.'</td>';
            }
            echo '</tr>';
        }
        echo '</table>';

    }
    private function readCSV($csvFile) {
        $file_handle = fopen($csvFile, 'r');
        while (!feof($file_handle)) {
            $line_of_text[] = fgetcsv($file_handle, 2048);
        }
        fclose($file_handle);

        return $line_of_text;
    }
    private function readCSV2($data) {
        

        $array_csv = array();
          $lines = explode("\n", $data);
          foreach ($lines as $line) {
            $array_csv[] = str_getcsv($line);
          }
          return $array_csv;
        
    }
    public function start($data,$test) {
       
        $csv     = $this->readCSV2($data);
        var_dump($csv);
        // $test     = $this->readCSV2($test);
        // $i       = 0; $z=1;
    }
    public function start_old($data,$test) {
        $a = []; //data
        $b = []; //labels

        // $csvFile = 'D:/ai/test.csv';
        $csv     = $this->readCSV2($data);
        $test     = $this->readCSV2($test);
        $i       = 0; $z=1;
        for ($k = 0; $k < count($csv); $k++) {
            $c = $csv[$k];

            for ($s=1; $s < count($c); $s++) { 
            	$a[$i][] = (int) $c[$s];
            }

            $b[$i] = $c[count($c)-1];
            // var_dump($c[count($c)-1]);

            // //pre marks
            // $a[$i][] = (int) $c['1'];
            // $a[$i][] = (int) $c['2'];
            // //atendance
            // $a[$i][] = (int) $c['3'];
            // $a[$i][] = (int) $c['4'];
            // $a[$i][] = (int) $c['5'];
            // $a[$i][] = (int) $c['6'];
            // $a[$i][] = (int) $c['7'];
            // $a[$i][] = (int) $c['8'];
            // //internal marks
            // $a[$i][] = (int) $c['9'];
            // $a[$i][] = (int) $c['10'];
            // $a[$i][] = (int) $c['11'];
            // $a[$i][] = (int) $c['12'];
            // $a[$i][] = (int) $c['13'];
            // $a[$i][] = (int) $c['14'];

            // $b[$i] = $c['15'];

            $i++; $z++;
            # code...
        }
        $j = 0;
        for ($k = 0; $k < count($test); $k++) {
            $c = $test[$k];

            for ($i=1; $i < count($c); $i++) { 
            	$d[$j][] = (int) $c[$i];
            }

            //pre marks
            // $d[$j][] = (int) $c['1'];
            // $d[$j][] = (int) $c['2'];
            // //atendance
            // $d[$j][] = (int) $c['3'];
            // $d[$j][] = (int) $c['4'];
            // $d[$j][] = (int) $c['5'];
            // $d[$j][] = (int) $c['6'];
            // $d[$j][] = (int) $c['7'];
            // $d[$j][] = (int) $c['8'];
            // //internal marks
            // $d[$j][] = (int) $c['9'];
            // $d[$j][] = (int) $c['10'];
            // $d[$j][] = (int) $c['11'];
            // $d[$j][] = (int) $c['12'];
            // $d[$j][] = (int) $c['13'];
            // $d[$j][] = (int) $c['14'];

            //$b[$i] = $c['15'];

            $j++;
            # code...
        }

        $classifier = new NaiveBayes();
        $classifier->train($a, $b);

        // $kmeans = new KMeans(2);
        // $r = $kmeans->cluster($a);
        // var_dump($r);

        //$d = $classifier->predict([75, 78, 100, 100, 100, 100, 90, 95, 20, 60, 20, 19, 20, 19]); //subiran
        //$d = $classifier->predict([80, 76, 50, 43, 60, 70, 10, 60, 8, 40, 10, 8, 8, 8]); //bipin
        $e = $classifier->predict($d); //bipin
        $t=[];
        // var_dump($test,$test[0][0],$test[1][0]);
        // for ($i=0; $i < count($test); $i++) { 
        // 	$t[$test[$i][0]] = $e[$i];
        // 	$i++;
        // }
        $w = 0;

        foreach ($e as $b) {
        	$t[$test[$w][0]] = $b;
            $x[$w][] =$b;
        	// var_dump($b);
        	$w++;
        }
        var_dump($e);

        // var_dump($t,$test)

        $kmeans = new KMeans(2);
        $r = $kmeans->cluster($x);
        var_dump($r);

        // var_dump($t);
        
        return $t;
    }

    public function index() {
        $this->load->model('entity');

        $login_rules = array(
            array(
                'field' => 'semester',
                'label' => 'Semester',
                'rules' => 'required',
            ),
        );


         // var_dump($this->input->post());
         //    $storagename = 'train01.csv';
         //    move_uploaded_file($_FILES["file"]["tmp_name"], DEFAULTFCPATH."/" . $storagename);
         //    var_dump($_FILES);

        $this->form_validation->set_rules($login_rules);

        if ($this->form_validation->run()) {
            $semester = $this->input->post('semester');
            $storagename = 'train01.csv';
            $storagename2 = 'test01.csv';
            move_uploaded_file($_FILES["file"]["tmp_name"], DEFAULTFCPATH."/" . $storagename);
            move_uploaded_file($_FILES["file2"]["tmp_name"], DEFAULTFCPATH."/" . $storagename2);

            
            $this->process($storagename,$semester,'train');
            $this->process($storagename2,$semester,'test');
            $this->student->makePredict($semester);
            // exit();

            redirect(site_url('home/predict/'.$semester),'refresh');

            // var_dump($data);
            //$data = fgetcsv($handle, 1000, ",");

            //$data = $this->input->post('data');
            // $test = $this->input->post('test');
            // $trained = $this->start($data,$test);


        } elseif (validation_errors()) {

            $this->template->set('error_message', '<ul>' . validation_errors('<li>', '</li>') . '</ul>');
            $this->template->set('validation_errors', validation_errors_array());
        }

        $title = 'Student Performance Analysis';
        //$trained = $this->start();
        $this->registry->set('title', $title);
        $this->_set_header_icon('dashboard');
        $this->load->model('clas');
        $class = $this->clas->get_all();
        $class = array_column($class, 'name','id');
        $class[0] = 'Select';
        // var_dump($class);

        //$entities = $this->entity->where('is_result',0)->get_all();
        //$results = $this->entity->where('is_result',1)->get_all();
        // var_dump($entities);

        $this->template
            ->prepend_title($title)
            ->set(compact('trained','class'))
            ->set_partial('scripts', 'home_scripts')
            ->build('home');
    }

}
