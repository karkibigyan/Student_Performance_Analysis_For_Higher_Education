<?php defined('BASEPATH') OR exit('No direct script access allowed.');
// use Phpml\Classification\NaiveBayes;

class Student_controller extends Base_Authenticated_Controller {

    public function __construct() {

        parent::__construct();

        $this->_set_nav('student');
        $this->load->library('grocery_crud')->model('clas');
    }

    public function index()
    {
    	$title = 'Student';
    	$this->registry->set('title', $title);
    	$this->_set_header_icon('dashboard');

    	$crud = new grocery_CRUD();
    	 
    	// Seriously! This is all the code you need!
    	$crud->set_table('student');
    	$crud->set_subject('Student');
        $crud->set_relation('class_id','class','name');
        $crud->display_as('class_id','Class');
    	 
    	$output = $crud->render();
        $classes = $this->clas->get_all();

    	$this->template
    	    ->prepend_title($title)
    	    ->set(compact('title','classes'))
    	    ->set('output',$output->output)
    	    ->set('css_files',$output->css_files)
    	    ->set('js_files',$output->js_files)
    	    ->set_partial('scripts', 'scripts')
    	    ->build('index');
    }
}