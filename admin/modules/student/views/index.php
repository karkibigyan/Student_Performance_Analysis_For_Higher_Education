<?php 
foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>

<?php echo Modules::run('feedback_messages_widget/index', array('with_javascript' =>false));?>
<div class="ui top attached tabular menu">
  <a class="item active" data-tab="third">Single</a>
  <a class="item" data-tab="second">Bulk</a>
</div>


<div class="ui bottom attached tab segment active" data-tab="third">
  

    <div class="col-md-12">
        <?php echo  $output; ?>
    </div>


</div>


               

<div class="ui bottom attached tab segment" data-tab="second">
    <div class="uis segments">

                <p>
                    <?php echo form_open('','class="ui form"');  ?>
    <div class="field">
          <label>Class</label>
          <select class="ui fluid dropdown">
            <option value="">Select</option>
            <?php foreach ($classes as $class): ?>
            <option value="<?php echo $class->code; ?>"><?php echo $class->name; ?></option>
            <?php endforeach; ?>
    </select>
    </div>
    <div class="field">

        <label for="date_picker_test">
            Bulk Student Import<br><small><i>Please Paste csv data.</i></small>
        </label>

        <div class="ui left icon input">
            <textarea name="data" id="" cols="30" rows="10"></textarea>
        </div>

    </div>
    <div class="field">
                            <p>
                                <button class="ui button" id="submit">Submit Data</button>
                            </p>
                        </div>
                        </form>
                    </p>
</div>