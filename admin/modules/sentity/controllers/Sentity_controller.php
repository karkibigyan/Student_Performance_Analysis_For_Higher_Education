<?php defined('BASEPATH') or exit('No direct script access allowed.');
// use Phpml\Classification\NaiveBayes;

class Sentity_controller extends Base_Authenticated_Controller
{

    public function __construct()
    {

        parent::__construct();

        $this->_set_nav('student');
        $this->load->library('grocery_crud')->model('entity');
    }

    public function index()
    {
        $title = 'Student Entity';
        $this->registry->set('title', $title);
        $this->_set_header_icon('dashboard');

        $crud = new grocery_CRUD();

        $crud->set_table('student_entity');
        $crud->set_subject('Student Entities');
        $crud->set_relation('student_id', 'student', 'full_name');
        $crud->display_as('student_id', 'Student');
        $crud->set_relation('entity_id', 'entity', 'name');
        $crud->display_as('entity_id', 'Entity');

        $output  = $crud->render();
        $entities = $this->entity->get_all();

        $this->template
            ->prepend_title($title)
            ->set(compact('title', 'entities'))
            ->set('output', $output->output)
            ->set('css_files', $output->css_files)
            ->set('js_files', $output->js_files)
            ->set_partial('scripts', 'scripts')
            ->build('index');
    }
}
