<?php defined('BASEPATH') OR exit('No direct script access allowed.');
// use Phpml\Classification\NaiveBayes;

class Entity_controller extends Base_Authenticated_Controller {

    public function __construct() {

        parent::__construct();

        $this->_set_nav('student');
        $this->load->library('grocery_crud')->model('clas');
    }

    public function index()
    {
    	$title = 'Entity';
    	$this->registry->set('title', $title);
    	$this->_set_header_icon('dashboard');

    	$crud = new grocery_CRUD();
    	 
    	$crud->set_table('entity');
    	$crud->set_subject('Entities');

    	 
    	$output = $crud->render();
        $classes = $this->clas->get_all();

    	$this->template
    	    ->prepend_title($title)
    	    ->set(compact('title','classes'))
    	    ->set('output',$output->output)
    	    ->set('css_files',$output->css_files)
    	    ->set('js_files',$output->js_files)
    	    ->set_partial('scripts', 'scripts')
    	    ->build('index');
    }
}