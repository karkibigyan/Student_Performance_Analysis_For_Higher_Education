<?php
defined('BASEPATH') or exit('No direct script access allowed');
use Phpml\Classification\NaiveBayes;
use Phpml\Metric\Accuracy;
use Phpml\Metric\ClassificationReport;

class Student extends Core_Model
{

    public $protected_attributes = array('id');
    protected $_table            = 'student_data';
    public function __construct()
    {
        parent::__construct();
    }

    public function predictetdResult($semester)
    {
        return $this->db->select('student.full_name,student.roll_no,final_result.result,student.id as student_id')->join('student','student.id = final_result.student_id')->where('final_result.semester_id',$semester)->order_by('student.full_name','asc')->get('final_result')->result();
    }

    public function getPredict($semester_id)
    {
    	// $predict = $this->makePredict($semester_id);
    	// exit();
        $train = $this->getTrainData($semester_id);
        return $train;
    }
    public function getHistoryLineGraph($id)
    {
    	$semester = $this->db->where('class.id',$id)->get('class')->row();
    	$semesterss = $this->db->select('class.id,class.name,year.name as yname')->join('year','year.id = class.year_id')->where('class.code',$semester->code)->get('class')->result_array();
    	$semesters = array_column($semesterss, 'id');
    	$i = 0;
    	foreach ($semesters as $sem) {
    		$d = $this->getData($sem);
    		$data[$semesterss[$i]['name'].' - '.$semesterss[$i]['yname']]['pass'] = $d['pass'];
    		$data[$semesterss[$i]['name'].' - '.$semesterss[$i]['yname']]['fail'] = $d['fail'];
    		$pass[] = $d['pass'];
    		$fail[] = $d['fail'];
    	$i++;
    	}
    	$data['labels'] = array_keys($data);
    	$data['pass'] = $pass;
    	$data['fail'] = $fail;
    	// var_dump($data);
    	return $data;

    }
    public function makePredict($semester_id)
    {
    	$t = $this->db->select('student_data.*,predicted_result.result')->where('student_data.type', 'train')->where('student_data.semester_id', $semester_id)->join('predicted_result', 'predicted_result.id = student_data.predection_id')
            // ->order_by('student_id')
            ->get('student_data')->result();
        $test = $this->db->select('result.*')
            ->where('result.semester_id', $semester_id)
            // ->order_by('student_id')
            ->get('result')->result();
        foreach ($t as $d) {
            $r[$d->student_id][] = (int) $d->value;
            $s[$d->student_id]   = $d->result;
        }
        foreach ($test as $te) {
            $tes[$te->student_id][] = (int) $te->value;
        }
        $rolls = array_keys($tes);
        foreach ($tes as $te) {
        	$newpredict[] = $te;
        }

        $labels = array_values($s);
        $data = array_values($r);

        $classifier = new NaiveBayes();
        $classifier->train($data, $labels);
       	$results = $classifier->predict($newpredict);
       	$i = 0;
       	foreach ($results as $r) {
       		$db[] = array('type'=>'test','student_id'=>$rolls[$i],'semester_id'=>$semester_id,'result'=>$r);
       		$i++;
       	}
       	// $this->db->insert('final_result', $db);
       	$this->db->insert_batch('final_result', $db);

       	

       // var_dump($f);

        



    }
    public function getTrainData($semester_id)
    {



    	$data['yes']   = 36;
        $data['no']    = 35;
        $data['final'] = [];
        return $data;
    }
    public function getTrainData_old($semester_id, $type)
    {
        $r = [];
        $s = [];
        $t = $this->db->select('student_data.*,predicted_result.result')->where('student_data.type', $type)->where('student_data.semester_id', $semester_id)->join('predicted_result', 'predicted_result.id = student_data.predection_id')
            ->order_by('of')
            ->get('student_data')->result();
        $test = $this->db->select('student_data.*')->where('student_data.type', 'test')
            ->where('student_data.semester_id', $semester_id)
            ->order_by('of')
            ->get('student_data')->result();
        $result = $this->db->select('predicted_result.*,student.full_name')->where('type', 'train')->where('semester_id', $semester_id)
            ->join('student', 'student.id = predicted_result.student_id')
            ->get('predicted_result')->result();
        foreach ($result as $rew) {
            $opt[$rew->full_name] = '';
        }
       
        
        $i   = 0;
        foreach ($t as $d) {
            $r[$d->student_id][] = (int) $d->value;
            $s[$d->student_id]   = $d->result;
        }


        $s = array_values($s);
        $r = array_values($r);

        $classifier = new NaiveBayes();
        $classifier->train($r, $s);
        $e = $classifier->predict($tes);
        $re1 = array_values($e);
        $re2 = array_keys($opt);

        foreach ($test as $te) {
            $tes[$te->student_id][] = (int) $te->value;
        }
        $tes = array_values($tes);


        for ($i = 0; $i < count($re1); $i++) {
            $final[$re2[$i]] = $re1[$i];
        }
       

        $i = 0;
        

        $arr1 = array_filter($e, function ($v) {return strpos($v, 'NO') === false;});
        
        $arr2 = array_diff($e, $arr1);
        $data['yes']   = count($arr1);
        $data['no']    = count($arr2);
        $data['final'] = $final;
        return $data;
    }

    public function getAccuracy($semester_id)
    {
        $t = $this->getData($semester_id);
        // var_dump($t);
        $train = $t['train'];
        $test = $t['test'];
        $data['has_accuracy'] = true;// (!empty(array_filter($test))) ? true : false;
        $data['default']      = Accuracy::score($train, $test);
        $data['normalized']   = Accuracy::score($train, $test, false);
        $data['total']        = count($test);
        $data['pass']        = $t['pass'];
        $data['fail']        = $t['fail'];

        $report = new ClassificationReport($train, $test);

        $data['precision'] = $report->getPrecision();

        $data['recall'] = $report->getRecall();

        $data['f1score'] = $report->getF1score();

        $data['support'] = $report->getSupport();

        $data['average'] = $report->getAverage();

         // var_dump($data);

        return $data;
        // exit();
    }
    public function getData($semester_id)
    {
    	$pass = $fail = 0;
    	$testData = $this->db->select('final_result.result as predicted_result,predicted_result.result as train_data')->where('final_result.semester_id',$semester_id)->join('predicted_result','predicted_result.student_id = final_result.student_id')->get('final_result')->result_array();
    	foreach ($testData as $t) {
    		$train[] = $t['train_data'];
    		$test[] = $t['predicted_result'];
    		if($t['predicted_result']=='YES'){
    			$pass += 1;
    		}else{
    			$fail += 1;
    		}
    	}
    	$data['train'] = $train;
    	$data['test'] = $test;
    	$data['pass'] = $pass;
    	$data['fail'] = $fail;
    	return $data;
    }
    public function getData_old($semester_id, $type)
    {
    	$db = ($type=='train') ? 'predicted_result' : 'final_result';
        $t = $this->db->select('result')->where('type', $type)->where('semester_id', $semester_id)->get($db)->result_array();
        // var_dump($t);
        return array_column($t, 'result');
    }

    public function getColumns()
    {
        $t = $this->db->select('of')->group_by('of')->get('student_data')->result_array();
        return array_column($t, 'of');
    }

    public function add($data, $semester_id = '1', $type = "train")
    {
        $firstrow = $data[0];
        unset($data[0]);
        foreach ($data as $d) {
            if (!empty($d) && !empty($d[0]) && !empty($d[1])) {
                $t         = count($d);
                $id        = 0;
                $student   = [];
                $predicted = 0;
                // $student = array(
                //     'full_name'=>$d[1],
                //     'roll_no'=>$d[0]
                // );
                //  $this->db->insert('student', $student);
                // $id = $this->db->insert_id();

                $id = $this->insertStudent($d[0], $d[1], $type);
                if ($type == 'train') {
                	$this->db->insert('predicted_result',
                        array('student_id' => $id,
                            'result'           => $d[$t - 1],
                            'semester_id'      => $semester_id,
                        ));
                $predicted = $this->db->insert_id();
                unset($d[0]);unset($d[1]);
                unset($firstrow[0]);unset($firstrow[1]);
                $total = ($type == 'test') ? count($d) : count($d) - 1;
                for ($i = 0; $i < $total; $i++) {
                    $r[$id][] = array(
                        'student_id'    => $id,
                        'value'         => $d[$i + 2],
                        'of'            => $firstrow[$i + 2],
                        'semester_id'   => $semester_id,
                        'type'          => $type,
                        'predection_id' => $predicted,
                    );
                }
                $this->db->insert_batch('student_data', $r[$id]);
                    
                } 
                else {
                	// $this->db->insert('predicted_result',
                 //        array('student_id' => $id,
                 //            'result'           => (empty($d[$t - 1])) ? null : $d[$t - 1],
                 //            'semester_id'      => $semester_id,
                 //            'type'             => 'test',
                 //        ));
                	// $predicted = $this->db->insert_id();
                	unset($d[0]);unset($d[1]);
                	unset($firstrow[0]);unset($firstrow[1]);
                	$total = ($type == 'test') ? count($d) : count($d) - 1;
                	for ($i = 0; $i < $total; $i++) {
                	    $r[$id][] = array(
                	        'student_id'    => $id,
                	        'value'         => $d[$i + 2],
                	        'of'            => $firstrow[$i + 2],
                	        'semester_id'   => $semester_id,
                	        'type'          => $type,
                	    );
                	}
                	$this->db->insert_batch('result', $r[$id]);
                    
                }

                

            }
        }
    }
    public function insertStudent($roll, $name, $type = 'train')
    {
        $r = null;
        if ($type == 'train') {
            if (!empty($roll) && !empty($name)) {
                $this->db->insert('student', array('full_name' => $name, 'roll_no' => $roll));
                $r = $this->db->insert_id();
            }
        } else {
            $i = $this->db->where('roll_no', $roll)->get('student')->row();
            if (!empty($i)) {
                $r = $i->id;
            } else {
                return $this->insertStudent($roll, $name, 'train');
            }
        }
        return $r;
    }
    // var_dump($st);
    //exit();
    // }

}

/* End of file Student.php */
/* Location: ./applications/admin/models/Student.php */
