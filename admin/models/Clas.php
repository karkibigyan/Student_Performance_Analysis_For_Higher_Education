<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clas extends Core_Model {

	public $protected_attributes = array('id');
	protected $_table = 'class';
	public function __construct(){
		parent::__construct();
	}
	public function get_all()
	{
		return $this->db->select("class.id, CONCAT_WS(' - ',class.name,year.name) as name")->join('year','year.id = class.year_id')->get('class')->result();
	}

}

/* End of file Class.php */
/* Location: ./applications/admin/models/Class.php */