<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Entity extends Core_Model {

	public $protected_attributes = array('id');
	protected $_table = 'entity';
	public function __construct(){
		parent::__construct();
	}

}

/* End of file Class.php */
/* Location: ./applications/admin/models/Class.php */